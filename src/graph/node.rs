use slotmap::Key;
slotmap::new_key_type! { pub struct NodeKey; }

/// Trait for objects which are uniquely identifiable by a usize value.
pub trait Identifiable {
    /// Returns the id of the current object. The returned id should be unique.
    fn id(&self) -> usize;
}

impl Identifiable for usize {
    fn id(&self) -> usize {
        *self
    }
}

/// Trait describing anything that can be used as a node weight.
pub trait Node: Eq {
    type Key: Key;
}

impl<T: Eq> Node for T {
    type Key = NodeKey;
}
