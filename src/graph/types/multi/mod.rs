use itertools::Itertools;

use crate::graph::accessor::{Edges, EdgesMut, Nodes, NodesMut};
use crate::graph::Directedness;
use crate::graph::edge::{Edge, EdgeKey, EdgeRef, HalfEdge};
use crate::graph::node::{Node, NodeKey};
use crate::storage::adj_list::AdjacencyList;
use crate::storage::StorageError;

pub struct MultiGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    storage: AdjacencyList<D, N, E>,
}

impl<D, N, E> Nodes<N> for MultiGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn n(&self) -> usize {
        self.storage.n()
    }

    fn node_data(
        &self,
        node: NodeKey,
    ) -> Result<&N, StorageError> {
        self.storage.node_data(node)
    }

    fn has_node(&self, node: NodeKey) -> bool {
        self.storage.node_data(node).is_ok()
    }

    fn nodes(&self) -> Box<dyn Iterator<Item = NodeKey> + '_> {
        self.storage.nodes()
    }
}

impl<D, N, E> NodesMut<N> for MultiGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn new() -> Self {
        MultiGraph {
            storage: AdjacencyList::new(),
        }
    }

    fn add_node(
        &mut self,
        data: N,
    ) -> NodeKey {
        self.storage.add_node(data)
    }

    fn get_or_add_node(
        &mut self,
        data: N,
    ) -> NodeKey {
        self.storage.get_or_add_node(data)
    }

    fn with_nodes_and_mapping<F>(
        num_nodes: usize,
        map: F,
    ) -> Self
    where
        F: Fn(usize) -> N,
    {
        Self {
            storage: AdjacencyList::with_nodes(num_nodes, map),
        }
    }
}

impl<D, N, E> Edges<D, E> for MultiGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn m(&self) -> usize {
        self.storage.m()
    }

    fn edges(&self) -> Box<dyn Iterator<Item = EdgeRef> + '_> {
        self.storage.edges()
    }

    fn weight_of(
        &self,
        e: EdgeKey,
    ) -> Result<&E, StorageError> {
        self.storage.weight_of(e)
    }

    fn out_edges(
        &self,
        u: NodeKey,
    ) -> Result<Box<dyn Iterator<Item = HalfEdge> + '_>, StorageError> {
        self.storage.out_edges(u)
    }
}

impl<D, N, E> EdgesMut<D, E> for MultiGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn add_edge(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
    ) -> Result<EdgeKey, StorageError> {
        self.storage.add_edge(u, v, data)
    }

    fn add_or_update_edge<F>(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
        update: F,
    ) -> Result<EdgeKey, StorageError>
    where
        F: Fn(&mut E),
    {
        self.storage.add_or_update_edge(u, v, data, update)
    }
}
