use itertools::Itertools;

use crate::graph::accessor::{Edges, EdgesMut, Nodes, NodesMut};
use crate::graph::edge::{Edge, EdgeKey};
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use crate::storage::adj_list::AdjacencyList;
use crate::storage::StorageError;

/// A simple graph that does not allow multi-edges but allows self-loops.
pub struct SimpleGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    pub storage: AdjacencyList<D, N, E>,
}

impl<D, N, E> Nodes<N> for SimpleGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn n(&self) -> usize {
        self.storage.n()
    }

    fn node_data(
        &self,
        node: NodeKey,
    ) -> Result<&N, StorageError> {
        self.storage.node_data(node)
    }

    fn has_node(
        &self,
        node: NodeKey,
    ) -> bool {
        self.storage.node_data(node).is_ok()
    }

    fn nodes(&self) -> Box<dyn Iterator<Item = NodeKey> + '_> {
        self.storage.nodes()
    }

    fn node_pairs(&self) -> Box<dyn Iterator<Item = (NodeKey, NodeKey)> + '_> {
        Box::new(self.nodes().combinations(2).map(|comb| (comb[0], comb[1])))
    }
}

impl<D, N, E> NodesMut<N> for SimpleGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn new() -> Self {
        SimpleGraph {
            storage: AdjacencyList::new(),
        }
    }

    fn add_node(
        &mut self,
        data: N,
    ) -> NodeKey {
        self.storage.add_node(data)
    }

    fn with_nodes_and_mapping<F>(
        num_nodes: usize,
        map: F,
    ) -> Self
    where
        F: Fn(usize) -> N,
    {
        Self {
            storage: AdjacencyList::with_nodes(num_nodes, map),
        }
    }

    fn get_or_add_node(
        &mut self,
        data: N,
    ) -> NodeKey {
        self.storage.get_or_add_node(data)
    }
}

impl<D, N, E> Edges<D, E> for SimpleGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn m(&self) -> usize {
        self.storage.m()
    }

    fn edges(&self) -> Box<dyn Iterator<Item = (NodeKey, NodeKey, EdgeKey)> + '_> {
        self.storage.edges()
    }

    fn weight_of(
        &self,
        e: EdgeKey,
    ) -> Result<&E, StorageError> {
        self.storage.weight_of(e)
    }

    fn out_edges(
        &self,
        u: NodeKey,
    ) -> Result<Box<dyn Iterator<Item = (NodeKey, EdgeKey)> + '_>, StorageError> {
        self.storage.out_edges(u)
    }
}

impl<D, N, E> EdgesMut<D, E> for SimpleGraph<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    fn add_edge(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
    ) -> Result<EdgeKey, StorageError> {
        if !self.has_node(u) {
            return Err(StorageError::InvalidNodeKey(u));
        }

        if !self.has_node(v) {
            return Err(StorageError::InvalidNodeKey(v));
        }

        if self.has_edge_between(u, v)? {
            panic!(
                "Graph already contains an edge between nodes {:?} and {:?}",
                u, v
            );
        }

        self.storage.add_edge(u, v, data)
    }

    fn add_or_update_edge<F>(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
        update: F,
    ) -> Result<EdgeKey, StorageError>
    where
        F: Fn(&mut E),
    {
        Ok(match self.first_edge_between(u, v) {
            Ok(Some(edge)) => {
                update(self.storage.edge_mut(edge).unwrap());
                edge
            }
            _ => self.add_edge(u, v, data)?,
        })
    }
}
