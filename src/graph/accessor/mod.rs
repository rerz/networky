use itertools::Itertools;
use rayon::iter::{IntoParallelRefIterator, ParallelBridge, ParallelIterator};

use crate::graph::edge::{Edge, EdgeKey, EdgeRef, HalfEdge};
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use crate::storage::StorageError;

pub trait Nodes<N>
where
    N: Node,
{
    /// Returns the number of nodes in the graph.
    fn n(&self) -> usize;

    // Returns the data associated with a given `NodeKey`
    fn node_data(
        &self,
        node: NodeKey,
    ) -> Result<&N, StorageError>;

    fn has_node(
        &self,
        node: NodeKey,
    ) -> bool;

    /// Returns an iterator over all nodes in the graph.
    fn nodes(&self) -> Box<dyn Iterator<Item = NodeKey> + '_>;

    /// Returns an iterator over all unique node pairs in the graph (only `(a, b)` but not `(b, a)`).
    fn node_pairs(&self) -> Box<dyn Iterator<Item = (NodeKey, NodeKey)> + '_> {
        Box::new(self.nodes().combinations(2).map(|comb| (comb[0], comb[1])))
    }
}

pub trait NodesMut<N>: Nodes<N>
where
    N: Node,
{
    fn new() -> Self;

    fn add_node(
        &mut self,
        data: N,
    ) -> NodeKey;

    fn with_nodes_and_mapping<F>(
        num_nodes: usize,
        map: F,
    ) -> Self
    where
        F: Fn(usize) -> N;

    fn get_or_add_node(
        &mut self,
        data: N,
    ) -> NodeKey;
}

pub trait Edges<D, E>
where
    D: Directedness,
    E: Edge,
{
    fn m(&self) -> usize;

    fn has_edge_between<N>(
        &self,
        u: NodeKey,
        v: NodeKey,
    ) -> Result<bool, StorageError>
    where
        Self: Nodes<N>,
        N: Node,
    {
        if !self.has_node(u) {
            return Err(StorageError::InvalidNodeKey(u));
        }

        if !self.has_node(v) {
            return Err(StorageError::InvalidNodeKey(v));
        }

        Ok(self
            .out_edges(u)?
            .find(|(target, e)| *target == v)
            .is_some())
    }

    fn first_edge_between<N>(
        &self,
        u: NodeKey,
        v: NodeKey,
    ) -> Result<Option<EdgeKey>, StorageError>
    where
        Self: Nodes<N>,
        N: Node,
    {
        if !self.has_node(u) {
            return Err(StorageError::InvalidNodeKey(u));
        }

        if !self.has_node(v) {
            return Err(StorageError::InvalidNodeKey(v));
        }

        Ok(self
            .out_edges(u)?
            .find_map(|(target, e)| if target == v { Some(e) } else { None }))
    }

    fn edges(&self) -> Box<dyn Iterator<Item = EdgeRef> + '_>;

    fn weight_of(
        &self,
        e: EdgeKey,
    ) -> Result<&E, StorageError>;

    fn total_edge_weight(&self) -> f64
    where
        Self: Sized,
    {
        for (u, v, e) in self.edges() {
            dbg!(e.float_weight(self).unwrap());
        }

        self.edges()
            .map(|(u, v, e)| {
                e.float_weight(self).expect(
                    "This should always be fine since we just got the edge key from the graph",
                )
            })
            .sum()
    }

    fn edges_between<'a, N>(
        &'a self,
        u: NodeKey,
        v: NodeKey,
    ) -> Result<Box<dyn Iterator<Item = EdgeRef> + '_>, StorageError>
    where
        D: 'a,
        E: 'a,
        N: Node,
        Self: Nodes<N>,
    {
        if !self.has_node(u) {
            return Err(StorageError::InvalidNodeKey(u));
        }

        if !self.has_node(v) {
            return Err(StorageError::InvalidNodeKey(u));
        }

        Ok(Box::new(self.edges().filter(move |(source, target, _)| {
            *source == u && *target == v || *source == v && *target == u
        })))
    }

    fn out_edges(
        &self,
        u: NodeKey,
    ) -> Result<Box<dyn Iterator<Item = HalfEdge> + '_>, StorageError>;

    fn weighted_out_degree(
        &self,
        u: NodeKey,
        self_loops_twice: bool,
    ) -> Result<f64, StorageError>
    where
        Self: Sized,
    {
        Ok(self
            .out_edges(u)?
            .map(|(v, e)| {
                let w = e.float_weight(self).unwrap();
                if self_loops_twice && u == v {
                    2.0 * w
                } else {
                    w
                }
            })
            .sum())
    }
}

pub trait EdgesMut<D, E>: Edges<D, E>
where
    D: Directedness,
    E: Edge,
{
    fn add_edge(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
    ) -> Result<EdgeKey, StorageError>;

    fn add_new_edge<N>(
        &mut self,
        u: N,
        v: N,
        data: E,
    ) -> EdgeKey
    where
        N: Node,
        Self: NodesMut<N> {
        let u = self.get_or_add_node(u);
        let v = self.get_or_add_node(v);
        self.add_edge(u, v, data).unwrap()
    }

    fn add_or_update_edge<F>(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
        update: F,
    ) -> Result<EdgeKey, StorageError>
    where
        F: Fn(&mut E);
}
