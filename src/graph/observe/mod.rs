use crate::graph::node::NodeKey;

pub trait NodeCentric {
    fn get_active_node(&self) -> Option<NodeKey>;
}

pub trait MultiNodeCentric {
    fn get_active_nodes(&self) -> Option<Vec<NodeKey>>;
}
