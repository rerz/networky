use crate::graph::accessor::Edges;
use crate::graph::node::NodeKey;
use crate::graph::Directedness;
use crate::storage::StorageError;
use slotmap::Key;

slotmap::new_key_type! { pub struct EdgeKey; }

impl EdgeKey {
    /// Helper for getting a float weight directly from an `EdgeKey`.
    pub fn float_weight<D, E, G>(
        &self,
        graph: &G,
    ) -> Result<f64, StorageError>
    where
        D: Directedness,
        E: Edge,
        G: Edges<D, E>,
    {
        Ok(graph.weight_of(*self)?.get_float_weight())
    }
}

/// Type alias for a tuple describing a full edge in a graph.
pub type EdgeRef = (NodeKey, NodeKey, EdgeKey);

/// Type alias for half of an edge in a graph. The `NodeKey` can either be the source or the target depending on the context.
pub type HalfEdge<N: Key = NodeKey, E: Key = EdgeKey> = (N, E);

/// A trait implemented by all types that can be used as edges in a graph.
pub trait Edge: Clone {
    type Key: Key;
    /// Returns some sort of float value representing the current edge's weight.
    /// Returns 1.0 by default as it is the most sensible for most algorithms.
    fn get_float_weight(&self) -> f64 {
        1.0
    }
}

impl<E> Edge for E
where
    E: Clone,
{
    type Key = EdgeKey;
}
