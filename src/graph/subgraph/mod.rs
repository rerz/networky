use crate::graph::node::{NodeKey, Node};
use crate::graph::accessor::{Nodes, Edges};
use crate::storage::StorageError;
use crate::graph::edge::{EdgeRef, EdgeKey, HalfEdge, Edge};
use crate::graph::Directedness;
use std::marker::PhantomData;

pub struct NodeInducedSubgraph<'graph, D, N, E, G> where D:  Directedness, N: Node, E: Edge, G: Nodes<N> + Edges<D, E> {
    super_graph: &'graph G,
    nodes: Vec<NodeKey>,
    _phantom: PhantomData<(D, N, E, G)>
}

impl<'graph, D, N, E, G> Nodes<N> for NodeInducedSubgraph<'graph, D, N, E, G> where D: Directedness, N: Node, E: Edge, G: Nodes<N> + Edges<D, E> {
    fn n(&self) -> usize {
        self.nodes.len()
    }

    fn node_data(&self, node: NodeKey) -> Result<&N, StorageError> {
        debug_assert!(self.nodes.contains(&node));

        self.super_graph.node_data(node)
    }

    fn has_node(&self, node: NodeKey) -> bool {
        self.nodes.contains(&node)
    }

    fn nodes(&self) -> Box<dyn Iterator<Item=NodeKey> + '_> {
        Box::new(self.nodes.iter().cloned())
    }
}

impl<'graph, D, N, E, G> Edges<D, E> for NodeInducedSubgraph<'graph, D, N, E, G> where D: Directedness, N: Node, E: Edge, G: Nodes<N> + Edges<D, E>  {
    fn m(&self) -> usize {
        // Way too expensive
        self.edges().count()
    }

    fn edges(&self) -> Box<dyn Iterator<Item=EdgeRef> + '_> {
        Box::new(self.node_pairs().flat_map(|(u, v)| self.super_graph.edges_between(u, v).unwrap()))
    }

    fn weight_of(&self, e: EdgeKey) -> Result<&E, StorageError> {
        self.super_graph.weight_of(e)
    }

    fn out_edges(&self, u: NodeKey) -> Result<Box<dyn Iterator<Item=HalfEdge> + '_>, StorageError> {
        Ok(Box::new(self.super_graph.out_edges(u)?.filter(|(v, _)| self.nodes.contains(v))))
    }
}