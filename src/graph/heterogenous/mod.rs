use crate::graph::edge::Edge;
use crate::storage::adj_list::heterogeneous::NodeStorage;
use slotmap::Key;
use typemap_rev::TypeMapKey;

pub struct HeterogeneousGraph {}

pub trait HeterogeneousNode: TypeMapKey<Value = NodeStorage<Self>> + Sized + Eq {
    type Key: Key;
}

pub trait HeterogeneousEdge: Edge {
    type Key: Key;

    type Source: HeterogeneousNode;
    type Target: HeterogeneousNode;
}
