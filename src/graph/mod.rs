use std::ops::Deref;

use crate::storage::Storage;

pub mod accessor;
pub mod edge;
pub mod node;
pub mod observe;
pub mod types;
mod subgraph;
pub mod heterogenous;

/// Trait describing the "directedness" of a graph.
/// This can be used to provide different impls of an algorithm for undirected and directed graphs.
pub trait Directedness: Clone {
    /// Method for checking graph directedness at runtime.
    fn is_directed() -> bool;
}

/// Marker for undirected graphs
#[derive(Copy, Clone)]
pub struct Undirected;

impl Directedness for Undirected {
    fn is_directed() -> bool {
        false
    }
}

/// Marker for directed graphs
#[derive(Copy, Clone)]
pub struct Directed;

impl Directedness for Directed {
    fn is_directed() -> bool {
        true
    }
}
