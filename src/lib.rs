#![feature(iter_partition_in_place, trait_alias)]

#[macro_use]
extern crate tracing;

use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::Edge;
use crate::graph::node::Node;
use crate::graph::Directedness;

pub mod algo;
pub mod graph;
pub mod io;
pub mod storage;

#[macro_use]
pub mod util;
