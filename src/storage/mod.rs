use thiserror::Error;
use crate::graph::node::NodeKey;
use crate::graph::edge::EdgeKey;

pub mod adj_list;

#[derive(Error, Debug)]
pub enum StorageError {
    #[error("Invalid node key used: {0:?}")]
    InvalidNodeKey(NodeKey),
    #[error("Invalid edge key used: {0:?}")]
    InvalidEdgeKey(EdgeKey),
}

pub trait Storage<D, W> {
    fn new_from<S: Storage<D, W>>(other: S) -> Self;
}