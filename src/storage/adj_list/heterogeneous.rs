use crate::graph::heterogenous::{HeterogeneousEdge, HeterogeneousNode};
use slotmap::DenseSlotMap;
use std::marker::PhantomData;
use typemap_rev::{TypeMap, TypeMapKey};

pub struct HalfEdges {
    nodes: TypeMap,
}

#[derive(Default)]
pub struct NodeStorage<N: HeterogeneousNode> {
    nodes: DenseSlotMap<N::Key, N>,
    _phantom: PhantomData<N>,
}

#[derive(Default)]
pub struct HeterogeneousAdjList {
    nodes: TypeMap,
}

impl HeterogeneousAdjList {
    pub fn add_node<N: HeterogeneousNode>(
        &mut self,
        node: N,
    ) -> N::Key {
        if !self.nodes.contains_key::<N>() {
            self.nodes.insert::<N>(NodeStorage::<N>::default());
        }

        let storage: &mut NodeStorage<N> = self.nodes.get_mut::<N>().unwrap();

        storage.nodes.insert(node)
    }

    pub fn data_of<N: HeterogeneousNode>(
        &self,
        key: N::Key,
    ) -> &N {
        let storage: &NodeStorage<N> = self.nodes.get::<N>().unwrap();
        storage.nodes.get(key).unwrap()
    }

    // pub fn add_edge<E: HeterogeneousEdge>(
    //     &mut self,
    //     source: <E::Source as HeterogeneousNode>::Key,
    //     target: <E::Target as HeterogeneousNode>::Key,
    //     data: E,
    // ) {
    //     let storage: &mut NodeStorage<_> = self
    //         .nodes
    //         .get_mut::<<E::Source as HeterogeneousNode>::Key>()
    //         .unwrap();
    //
    //     let source = storage.nodes.get(source).unwrap();
    //     let target = storage.nodes.get(target).unwrap();
    // }
}

#[cfg(test)]
mod tests {
    use crate::graph::heterogenous::HeterogeneousNode;
    use crate::storage::adj_list::heterogeneous::{HeterogeneousAdjList, NodeStorage};
    use typemap_rev::TypeMapKey;

    #[test]
    fn test() {
        let mut adj_list = HeterogeneousAdjList::default();

        #[derive(Eq, PartialOrd, PartialEq)]
        struct SomeNode;

        slotmap::new_key_type! { struct SomeNodeKey; }

        impl TypeMapKey for SomeNode {
            type Value = NodeStorage<Self>;
        }

        impl HeterogeneousNode for SomeNode {
            type Key = SomeNodeKey;
        }

        let key = adj_list.add_node(SomeNode);

        let node = adj_list.data_of::<SomeNode>(key);
    }
}
