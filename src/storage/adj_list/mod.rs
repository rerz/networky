use std::marker::PhantomData;

use slotmap::{DenseSlotMap, Key, SecondaryMap};

use crate::graph::accessor::Edges;
use crate::graph::edge::{Edge, EdgeKey, EdgeRef, HalfEdge};
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use crate::storage::StorageError;

pub mod heterogeneous;

pub struct NodeStorage<N>
where
    N: Node,
{
    n: usize,
    node_data: DenseSlotMap<N::Key, N>,
}

impl<N: Node> Default for NodeStorage<N> {
    fn default() -> Self {
        Self {
            n: 0,
            node_data: DenseSlotMap::with_key(),
        }
    }
}

pub struct EdgeStorage<D, M, N, E>
where
    D: Directedness,
    M: Node,
    N: Node,
    E: Edge,
{
    m: usize,
    num_self_loops: usize,
    edge_data: DenseSlotMap<E::Key, E>,
    out_edges: SecondaryMap<M::Key, Vec<HalfEdge<N::Key, E::Key>>>,
    in_edges: SecondaryMap<M::Key, Vec<HalfEdge<N::Key, E::Key>>>,
    _phantom: PhantomData<D>,
}

impl<D, M, N, E> Default for EdgeStorage<D, M, N, E>
where
    D: Directedness,
    M: Node,
    N: Node,
    E: Edge,
{
    fn default() -> Self {
        Self {
            m: 0,
            num_self_loops: 0,
            edge_data: DenseSlotMap::with_key(),
            out_edges: SecondaryMap::new(),
            in_edges: SecondaryMap::new(),
            _phantom: PhantomData,
        }
    }
}

#[derive(Default)]
pub struct AdjacencyList<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    node_storage: NodeStorage<N>,
    edge_storage: EdgeStorage<D, N, N, E>,
}

impl<D, N, E> AdjacencyList<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    pub fn new() -> Self {
        Self {
            node_storage: NodeStorage::<N>::default(),
            edge_storage: EdgeStorage::<D, N, N, E>::default(),
        }
    }

    pub fn with_nodes<F>(
        num_nodes: usize,
        map: F,
    ) -> Self
    where
        F: Fn(usize) -> N,
    {
        todo!()
    }

    pub fn weight_of(
        &self,
        e: EdgeKey,
    ) -> Result<&E, StorageError> {
        //self.edge_data.get(e).ok_or(StorageError::InvalidEdgeKey(e))
    }

    pub unsafe fn weight_of_unchecked(
        &self,
        e: EdgeKey,
    ) -> &E {
        self.edge_data.get_unchecked(e)
    }

    pub fn node_data(
        &self,
        node: NodeKey,
    ) -> Result<&N, StorageError> {
        self.node_data
            .get(node)
            .ok_or(StorageError::InvalidNodeKey(node))
    }

    pub fn node_data_mut(
        &mut self,
        node: NodeKey,
    ) -> Result<&mut N, StorageError> {
        self.node_data
            .get_mut(node)
            .ok_or(StorageError::InvalidNodeKey(node))
    }

    // FIXME: highly inefficient
    pub fn get_or_add_node(
        &mut self,
        data: N,
    ) -> NodeKey {
        let key = self.node_data.iter().find_map(
            |(key, node_data)| {
                if *node_data == data {
                    Some(key)
                } else {
                    None
                }
            },
        );

        match key {
            Some(key) => key,
            None => self.add_node(data),
        }
    }

    pub fn add_node(
        &mut self,
        data: N,
    ) -> NodeKey {
        let key = self.node_data.insert(data);
        self.out_edges.insert(key, vec![]);
        self.in_edges.insert(key, vec![]);
        self.n += 1;
        key
    }

    pub fn edges(&self) -> Box<dyn Iterator<Item = EdgeRef> + '_> {
        if D::is_directed() {
            Box::new(
                self.out_edges
                    .iter()
                    .flat_map(|(u, edges)| edges.iter().map(move |(v, e)| (u, *v, *e))),
            )
        } else {
            Box::new(self.out_edges.iter().flat_map(|(u, edges)| {
                edges
                    .iter()
                    .filter(move |(v, e)| u >= *v)
                    .map(move |(v, e)| (u, *v, *e))
            }))
        }
    }

    pub fn out_edges(
        &self,
        u: NodeKey,
    ) -> Result<Box<dyn Iterator<Item = HalfEdge> + '_>, StorageError> {
        let out_edges = self
            .out_edges
            .get(u)
            .ok_or(StorageError::InvalidNodeKey(u))?;
        Ok(Box::new(out_edges.iter().cloned()))
    }

    pub fn add_edge(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
    ) -> Result<EdgeKey, StorageError> {
        let key = self.edge_data.insert(data);

        self.out_edges
            .get_mut(u)
            .ok_or(StorageError::InvalidNodeKey(u))?
            .push((v, key));

        if u == v {
            self.num_self_loops += 1;
        } else {
            if D::is_directed() {
                self.in_edges
                    .get_mut(v)
                    .ok_or(StorageError::InvalidNodeKey(u))?
                    .push((u, key));
            } else {
                self.out_edges
                    .get_mut(v)
                    .ok_or(StorageError::InvalidNodeKey(u))?
                    .push((u, key));
            }
        }

        self.m += 1;

        Ok(key)
    }

    pub fn edge_mut(
        &mut self,
        e: EdgeKey,
    ) -> Option<&mut E> {
        self.edge_data.get_mut(e)
    }

    pub fn n(&self) -> usize {
        self.n
    }

    pub fn m(&self) -> usize {
        self.m
    }

    pub fn nodes(&self) -> Box<dyn Iterator<Item = NodeKey> + '_> {
        Box::new(self.node_data.keys())
    }

    // FIXME: linear search through out edges makes this pretty slow
    pub fn add_or_update_edge<F>(
        &mut self,
        u: NodeKey,
        v: NodeKey,
        data: E,
        update: F,
    ) -> Result<EdgeKey, StorageError>
    where
        F: Fn(&mut E),
    {
        Ok(
            if let Some((_, e)) = self
                .out_edges
                .get(u)
                .ok_or(StorageError::InvalidNodeKey(u))?
                .iter()
                .find(|(target, e)| *target == v)
            {
                let weight = self
                    .edge_data
                    .get_mut(*e)
                    .ok_or(StorageError::InvalidEdgeKey(*e))?;

                update(weight);

                *e
            } else {
                self.add_edge(u, v, data)?
            },
        )
    }
}
