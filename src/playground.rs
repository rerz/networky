use networky::graph::heterogenous::{HeterogeneousEdge, HeterogeneousNode};
use networky::graph::types::simple::SimpleGraph;
use networky::graph::Directed;

#[derive(Ord, PartialOrd, Eq, PartialEq)]
pub struct Artist;

slotmap::new_key_type! {
    pub struct ArtistKey;
}

#[derive(Clone, Copy)]
pub struct RelatedTo;

slotmap::new_key_type! {
    pub struct RelatedToKey;
}

impl HeterogeneousEdge for RelatedTo {
    type Key = RelatedToKey;

    type Source = Artist;
    type Target = Artist;
}

impl HeterogeneousNode for Artist {
    type Key = ArtistKey;
}

#[derive(Ord, PartialOrd, PartialEq, Eq)]
pub enum SpotifyNodes {
    Artist(Artist),
}

#[derive(Clone)]
pub enum SpotifyEdges {
    RelatedTo(RelatedTo),
}

pub struct SpotifyGraph {
    inner: SimpleGraph<Directed, SpotifyNodes, SpotifyEdges>,
}

fn main() {}
