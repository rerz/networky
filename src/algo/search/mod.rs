pub mod dfs;

pub enum NodeColor {
    White,
    Grey,
    Black,
}