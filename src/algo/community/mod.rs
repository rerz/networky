use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use std::ops::Deref;
use std::rc::Rc;

use itertools::{Either, Itertools};
use slotmap::{DenseSlotMap, SecondaryMap, SparseSecondaryMap};
use tap::Tap;
use thiserror::Error;

use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::{Edge, EdgeKey, EdgeRef};
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use crate::storage::StorageError;

pub mod clustering;
pub mod hierarchy;
pub mod louvain;
pub mod map_equation;
pub mod metrics;

#[derive(Error, Debug)]
pub enum ClusteringError {
    #[error("Invalid cluster key used: `{0:?}`")]
    InvalidClusterKey(ClusterKey),
}

slotmap::new_key_type! {
    pub struct ClusterKey;
}

pub struct IntraClusterEdges {
    pub edges: SecondaryMap<ClusterKey, Vec<EdgeRef>>,
}

pub struct InterClusterEdges {
    pub edges: Vec<EdgeRef>,
}

pub trait ClusteringObserver {
    fn on_cluster_changed(
        &mut self,
        u: NodeKey,
        old_cluster: ClusterKey,
        new_cluster: ClusterKey,
    );
}
