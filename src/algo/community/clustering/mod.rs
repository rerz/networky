use std::collections::HashSet;

use itertools::Itertools;

use crate::algo::community::{ClusteringError, ClusterKey};
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::Directedness;
use crate::graph::edge::{Edge, EdgeRef};
use crate::graph::node::{Node, NodeKey};
use crate::storage::StorageError;

pub mod observable;
pub mod standard;

pub trait ClusteringAlgorithm<D, N, E, G, C>
where
    D: Directedness,
    N: Node,
    E: Edge,
    G: Nodes<N> + Edges<D, E>,
    C: Clustering,
{
    type Config;

    fn new_with_clustering(clustering: C) -> Self;

    fn new_with_singleton_clustering(graph: &G) -> Self
    where
        Self: Sized,
    {
        Self::new_with_clustering(C::singleton_from(graph.nodes()))
    }

    fn clustering(&self) -> &C;

    fn into_clustering(self) -> C;

    fn run(
        &mut self,
        graph: &G,
        config: &Self::Config
    );
}

pub trait Clustering {
    fn new() -> Self;

    fn new_with(num_clusters: usize) -> Self
    where
        Self: Sized,
    {
        let mut clustering = Self::new();
        for _ in 0..num_clusters {
            clustering.new_cluster();
        }
        clustering
    }

    fn singleton_from<I>(nodes: I) -> Self
    where
        I: Iterator<Item = NodeKey>,
        Self: Sized,
    {
        let mut clustering = Self::new();
        for node in nodes {
            let key = clustering.new_cluster();
            clustering.set(node, key);
        }
        clustering
    }

    fn cluster_keys(&self) -> Box<dyn Iterator<Item = ClusterKey> + '_>;

    fn clusters(&self) -> Box<dyn Iterator<Item = (ClusterKey, &HashSet<NodeKey>)> + '_>;

    fn compact(&mut self);

    fn num_clusters(&self) -> usize;

    fn nodes_of(
        &self,
        cluster: ClusterKey,
    ) -> Result<Box<dyn Iterator<Item = NodeKey> + '_>, ClusteringError>;

    fn cluster_of(
        &self,
        node: NodeKey,
    ) -> Option<ClusterKey>;

    fn new_cluster(&mut self) -> ClusterKey;

    fn set(
        &mut self,
        node: NodeKey,
        cluster: ClusterKey,
    ) -> Result<Option<ClusterKey>, ClusteringError>;

    fn neighboring_clusters<D, N, E, G>(
        &self,
        graph: &G,
        u: NodeKey,
    ) -> Result<Vec<ClusterKey>, StorageError>
    where
        D: Directedness,
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
    {
        Ok(graph
            .out_edges(u)?
            .map(|(v, _)| self.cluster_of(v).unwrap())
            .chain(std::iter::once(self.cluster_of(u).unwrap()))
            .unique()
            .collect())
    }

    fn intra_and_inter_cluster_edges<D, N, E, G>(
        &self,
        graph: &G,
    ) -> (Vec<EdgeRef>, Vec<EdgeRef>)
    where
        D: Directedness,
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
    {
        graph
            .edges()
            .partition(|(u, v, _)| self.cluster_of(*u) == self.cluster_of(*v))
    }
}
