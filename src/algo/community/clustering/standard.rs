use std::cell::RefCell;
use std::collections::HashSet;
use std::rc::Rc;

use itertools::Itertools;
use slotmap::{DenseSlotMap, SparseSecondaryMap};

use crate::algo::community::clustering::Clustering;
use crate::algo::community::{ClusterKey, ClusteringError, ClusteringObserver};
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::{Edge, EdgeRef};
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use crate::storage::StorageError;

#[derive(Default, Clone)]
pub struct StandardClustering {
    clusters: DenseSlotMap<ClusterKey, HashSet<NodeKey>>,
    node_index: SparseSecondaryMap<NodeKey, ClusterKey>,
}

impl Clustering for StandardClustering {
    fn new() -> Self {
        Self {
            clusters: DenseSlotMap::with_key(),
            node_index: SparseSecondaryMap::new(),
        }
    }

    fn cluster_keys(&self) -> Box<dyn Iterator<Item = ClusterKey> + '_> {
        Box::new(self.clusters.keys())
    }

    fn clusters(&self) -> Box<dyn Iterator<Item = (ClusterKey, &HashSet<NodeKey>)> + '_> {
        Box::new(self.clusters.iter())
    }

    fn compact(&mut self) {
        self.clusters.retain(|_, nodes| !nodes.is_empty());
    }

    fn num_clusters(&self) -> usize {
        self.clusters.len()
    }

    fn nodes_of(
        &self,
        c: ClusterKey,
    ) -> Result<Box<dyn Iterator<Item = NodeKey> + '_>, ClusteringError> {
        self.clusters
            .get(c)
            .map(|nodes| -> Box<dyn Iterator<Item = NodeKey> + '_> {
                Box::new(nodes.iter().cloned())
            })
            .ok_or(ClusteringError::InvalidClusterKey(c))
    }

    fn cluster_of(
        &self,
        node: NodeKey,
    ) -> Option<ClusterKey> {
        self.node_index.get(node).cloned()
    }

    fn new_cluster(&mut self) -> ClusterKey {
        self.clusters.insert(HashSet::new())
    }

    fn set(
        &mut self,
        node: NodeKey,
        cluster: ClusterKey,
    ) -> Result<Option<ClusterKey>, ClusteringError> {
        let nodes = self
            .clusters
            .get_mut(cluster)
            .ok_or(ClusteringError::InvalidClusterKey(cluster))?;

        nodes.insert(node);

        let old = self.node_index.insert(node, cluster);

        if let Some(old) = old {
            // Safe because we know old has to be present in the clustering
            unsafe {
                self.clusters.get_unchecked_mut(old).remove(&node);
            }
        }

        Ok(old)
    }
}
