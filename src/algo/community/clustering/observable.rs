use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::algo::community::{ClusterKey, ClusteringError};
use crate::graph::node::NodeKey;
use futures::stream::FuturesOrdered;
use std::collections::HashSet;
use tokio::sync::broadcast;

#[derive(Clone, Debug)]
pub struct ClusterChange {
    pub node: NodeKey,
    pub from: Option<ClusterKey>,
    pub to: ClusterKey,
}

pub struct ObservableClustering<C> {
    inner: C,
    sender: broadcast::Sender<ClusterChange>,
    receiver: broadcast::Receiver<ClusterChange>,
}

impl<C> ObservableClustering<C> where C: Clustering {
    pub fn new_with_inner(inner: C) -> Self {
        let (sender, receiver) = broadcast::channel(1);
        Self {
            inner,
            sender,
            receiver
        }
    }

    pub fn get_receiver(&self) -> broadcast::Receiver<ClusterChange> {
        self.sender.subscribe()
    }
}

impl<C> Clustering for ObservableClustering<C> where C: Clustering {
    fn new() -> Self {
        let (sender, receiver) = broadcast::channel(1);
        Self {
            inner: C::new(),
            sender,
            receiver,
        }
    }

    fn cluster_keys(&self) -> Box<dyn Iterator<Item = ClusterKey> + '_> {
        self.inner.cluster_keys()
    }

    fn clusters(&self) -> Box<dyn Iterator<Item = (ClusterKey, &HashSet<NodeKey>)> + '_> {
        self.inner.clusters()
    }

    fn compact(&mut self) {
        self.inner.compact()
    }

    fn num_clusters(&self) -> usize {
        self.inner.num_clusters()
    }

    fn nodes_of(
        &self,
        cluster: ClusterKey,
    ) -> Result<Box<dyn Iterator<Item = NodeKey> + '_>, ClusteringError> {
        self.inner.nodes_of(cluster)
    }

    fn cluster_of(
        &self,
        node: NodeKey,
    ) -> Option<ClusterKey> {
        self.inner.cluster_of(node)
    }

    fn new_cluster(&mut self) -> ClusterKey {
        self.inner.new_cluster()
    }

    fn set(
        &mut self,
        node: NodeKey,
        cluster: ClusterKey,
    ) -> Result<Option<ClusterKey>, ClusteringError> {
        let old = self.inner.set(node, cluster)?;

        self.sender.send(ClusterChange {
            node,
            from: old,
            to: cluster,
        }).unwrap();

        Ok(old)
    }
}
