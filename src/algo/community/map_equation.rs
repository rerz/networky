use std::collections::HashMap;
use std::time::Instant;

use itertools::Itertools;
use slotmap::{SecondaryMap, SparseSecondaryMap};

use crate::algo::coarsen::ClusterCoarsening;
use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::{Clustering, ClusteringAlgorithm};
use crate::algo::community::ClusterKey;
use crate::algo::local_moving::sequential::{SequentialLocalMoving, SequentialLocalMovingConfig};
use crate::algo::local_moving::{
    LocalMovingObjective, LocalMovingOptimizer, OptimizationCriterion,
};
use crate::graph::accessor::{Edges, EdgesMut, Nodes, NodesMut};
use crate::graph::edge::Edge;
use crate::graph::node::{Identifiable, Node, NodeKey};
use crate::graph::{Directedness, Undirected};
use crate::io::dot_writer::DotWriter;
use std::marker::PhantomData;

pub struct MapEquationClustering<D, N, E, G, C: Clustering> {
    clustering: C,
    _phantom: PhantomData<(D, N, E, G)>,
}

pub struct MapEquationConfig {
    pub recursive: bool,
    pub max_iter: usize,
}

impl<N, E, G, C> ClusteringAlgorithm<Undirected, N, E, G, C>
    for MapEquationClustering<Undirected, N, E, G, C>
where
    N: Node,
    E: Edge,
    G: NodesMut<N> + EdgesMut<Undirected, E>,
    C: Clustering,
{
    type Config = MapEquationConfig;

    fn new_with_clustering(clustering: C) -> Self {
        Self {
            clustering,
            _phantom: PhantomData,
        }
    }

    fn clustering(&self) -> &C {
        &self.clustering
    }

    fn into_clustering(self) -> C {
        self.clustering
    }

    fn run(
        &mut self,
        graph: &G,
        config: &Self::Config,
    ) {
        #[cfg(debug_assertions)]
        let start = Instant::now();

        let mut optimizer = SequentialLocalMoving::with_initial_clustering(
            graph,
            &mut self.clustering,
            MapEquation::new(),
            SequentialLocalMovingConfig {
                max_iter: 100,
                randomize_node_order: false,
            },
            OptimizationCriterion::Minimize,
        );

        let changed = optimizer.move_nodes(graph);

        let mut clustering = optimizer.clustering;

        warn!("Compacting clustering after local moving.");
        clustering.compact();

        warn!(
            "Clustering contains {} clusters after local moving and compacting.",
            clustering.num_clusters()
        );

        if config.recursive && changed {
            warn!("Coarsening graph and recursing");
            let coarse = ClusterCoarsening::coarsen::<_, _, Vec<NodeKey>, C>(
                graph,
                &clustering,
                |node, nodes| nodes.push(node),
            )
            .unwrap();

            let mut on_coarse = MapEquationClustering::new_with_singleton_clustering(&coarse);

            on_coarse.run(&coarse, config.clone());

            let coarse_clustering: C = on_coarse.clustering;

            for (cluster, nodes) in coarse_clustering.clusters() {
                // Find all the fine nodes that the coarse nodes in the cluster map to
                let mut fine_nodes = nodes
                    .iter()
                    .flat_map(|u| coarse.node_data(*u).unwrap().iter());

                let fine_cluster = clustering.new_cluster();

                for node in fine_nodes {
                    clustering.set(*node, fine_cluster);
                }
            }
        } else if !changed {
            // We reached the top level, probably do something here.
        }

        if changed {
            warn!("Compacting after unpacking");
            clustering.compact();
        }

        #[cfg(debug_assertions)]
        {
            let end = Instant::now();
            tracing::info!("Clustering took: {}ms", (end - start).as_millis());
        }
    }
}

#[cfg(not(debug_assertions))]
pub struct MapEquation;

#[cfg(debug_assertions)]
pub struct MapEquation {
    debug_data: MapEquationDebugData,
}

#[cfg(debug_assertions)]
#[derive(Default)]
pub struct MapEquationDebugData {
    pub current_map_equation: f64,
    pub expected_map_equation: f64,
    pub last_delta: f64,
}

impl MapEquation {
    #[cfg(debug_assertions)]
    pub fn new() -> Self {
        Self {
            debug_data: Default::default(),
        }
    }

    #[cfg(not(debug_assertions))]
    pub fn new() -> Self {
        Self {}
    }

    fn p_log_p(
        x: f64,
        divisor: f64,
    ) -> f64 {
        if x <= 0.0 {
            return 0.0;
        }

        let p = x / divisor;
        p * p.log2()
    }

    pub fn calculate<G, N, E, C>(
        graph: &G,
        clustering: &C,
        total_cut: f64,
        total_vol: f64,
        cluster_cuts: &SecondaryMap<ClusterKey, f64>,
        cluster_vols: &SecondaryMap<ClusterKey, f64>,
    ) -> f64
    where
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<Undirected, E>,
        C: Clustering,
    {
        let mut sum_cut_plus_vol = 0.0;
        let mut sum_cut = 0.0;
        let mut sum_w_alpha = 0.0;

        for c in clustering.cluster_keys() {
            let cut = cluster_cuts[c];
            let vol = cluster_vols[c];

            sum_cut_plus_vol += Self::p_log_p(cut + vol, total_vol);
            sum_cut += Self::p_log_p(cut, total_vol);
        }

        for node in graph.nodes() {
            let vol = graph.weighted_out_degree(node, true).unwrap();
            sum_w_alpha += Self::p_log_p(vol, total_vol);
        }

        Self::p_log_p(total_cut, total_vol) - 2.0 * sum_cut + sum_cut_plus_vol
    }
}

/// Data associated with an entire move phase.
#[derive(Default)]
pub struct MapEquationMovePhaseData {
    pub total_cut: f64,
    pub total_vol: f64,
    pub cluster_cut: SecondaryMap<ClusterKey, f64>,
    pub cluster_vol: SecondaryMap<ClusterKey, f64>,
}

/// Data associated with one "round" of local moving.
#[derive(Default)]
pub struct MapEquationMoveIterationData {
    pub nodes_moved: usize,
}

/// Data associated with one move attempt
#[derive(Default)]
pub struct MapEquationMoveAttemptData {
    pub vol: f64,
    pub loops: f64,
    pub delta_to_current: f64,
    pub weight_to_cluster: SparseSecondaryMap<ClusterKey, f64>,
}

impl<N, E, G, C> LocalMovingObjective<Undirected, N, E, G, C> for MapEquation
where
    N: Node,
    E: Edge,
    G: Nodes<N> + Edges<Undirected, E>,
    C: Clustering,
{
    type MovePhaseData = MapEquationMovePhaseData;
    type MoveIterationData = MapEquationMoveIterationData;
    type MoveAttemptData = MapEquationMoveAttemptData;

    fn on_move_phase_started(
        &mut self,
        graph: &G,
        clustering: &C,
        move_phase_data: &mut Self::MovePhaseData,
    ) {
        info!("Calculating cluster cuts and volumes");
        for u in graph.nodes() {
            let c = clustering.cluster_of(u).unwrap();

            move_phase_data.cluster_cut.entry(c).unwrap().or_insert(0.0);
            move_phase_data.cluster_vol.entry(c).unwrap().or_insert(0.0);

            for (v, e) in graph.out_edges(u).unwrap() {
                let d = clustering.cluster_of(v).unwrap();

                move_phase_data.cluster_cut.entry(d).unwrap().or_insert(0.0);
                move_phase_data.cluster_vol.entry(d).unwrap().or_insert(0.0);

                let w = e.float_weight(graph).unwrap();

                // Inter-cluster edge
                if c != d {
                    move_phase_data.cluster_cut[c] += w;
                }

                // Count self loops twice
                move_phase_data.cluster_vol[c] += if u == v { 2.0 * w } else { w };
            }
        }

        warn!("Clustering contains {} clusters", clustering.num_clusters());

        for c in clustering.cluster_keys() {
            move_phase_data.total_cut += move_phase_data.cluster_cut[c];
            move_phase_data.total_vol += move_phase_data.cluster_vol[c];
        }

        warn!("Total cut: {}", move_phase_data.total_cut);
        warn!("Total vol: {}", move_phase_data.total_vol);
    }

    fn on_move_iteration_started(
        &mut self,
        graph: &G,
        clustering: &C,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
    ) {
    }

    fn on_try_move_node(
        &mut self,
        u: NodeKey,
        c: ClusterKey,
        graph: &G,
        clustering: &C,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
        move_attempt_data: &mut Self::MoveAttemptData,
    ) {
        move_attempt_data.weight_to_cluster = SparseSecondaryMap::new();

        #[cfg(debug_assertions)]
        {
            self.debug_data.current_map_equation = MapEquation::calculate(
                graph,
                clustering,
                move_phase_data.total_cut,
                move_phase_data.total_vol,
                &move_phase_data.cluster_cut,
                &move_phase_data.cluster_vol,
            );
        }

        let c = clustering.cluster_of(u).unwrap();

        move_attempt_data.weight_to_cluster.insert(c, 0.0);

        for (v, e) in graph.out_edges(u).unwrap() {
            let d = clustering.cluster_of(v).unwrap();
            let w = e.float_weight(graph).unwrap();

            if !move_attempt_data.weight_to_cluster.contains_key(d) {
                move_attempt_data.weight_to_cluster.insert(d, 0.0);
            }

            move_attempt_data.vol += w;

            if u != v {
                if c == d {
                    move_attempt_data.weight_to_cluster[c] += w;
                } else {
                    move_attempt_data.weight_to_cluster[d] += w;
                }
            } else {
                move_attempt_data.loops += w;
                move_attempt_data.vol += w;
            }
        }

        let delta_current = self.calculate_delta(
            u,
            c,
            c,
            graph,
            clustering,
            move_phase_data,
            move_iteration_data,
            move_attempt_data,
        );
        move_attempt_data.delta_to_current = delta_current;
    }

    fn on_node_moved(
        &mut self,
        u: NodeKey,
        from: ClusterKey,
        to: ClusterKey,
        graph: &G,
        clustering: &C,
        best_delta: f64,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
        move_attempt_data: &mut Self::MoveAttemptData,
    ) {
        tracing::debug!(
            "Node {:?} moved from cluster {:?} to cluster {:?}",
            u,
            from,
            to
        );

        move_iteration_data.nodes_moved += 1;

        let degree = move_attempt_data.vol;
        let loops = move_attempt_data.loops;

        let weight_to_source = move_attempt_data.weight_to_cluster[from];
        let weight_to_target = move_attempt_data.weight_to_cluster[to];

        // How did the cut of the "from" cluster change
        let cut_diff_source = 2.0 * weight_to_source - degree + 2.0 * loops;
        // How did the cut of the target cluster change
        let cut_diff_target = degree - 2.0 * weight_to_target - 2.0 * loops;

        move_phase_data.cluster_cut[from] += cut_diff_source;
        move_phase_data.cluster_cut[to] += cut_diff_target;

        move_phase_data.cluster_vol[from] -= degree;
        move_phase_data.cluster_vol[to] += degree;

        // ???
        move_phase_data.total_cut += cut_diff_source + cut_diff_target;

        #[cfg(debug_assertions)]
        {
            let me = MapEquation::calculate(
                graph,
                clustering,
                move_phase_data.total_cut,
                move_phase_data.total_vol,
                &move_phase_data.cluster_cut,
                &move_phase_data.cluster_vol,
            );

            let me_before_move = self.debug_data.current_map_equation;

            let expected = me_before_move + best_delta - move_attempt_data.delta_to_current;

            let diff = me - me_before_move;

            if !float_cmp::approx_eq!(f64, me, expected, epsilon = 0.00000005) {
                panic!("Calculated map equation does not match expected")
            }
        }
    }

    fn on_move_iteration_ended(
        &mut self,
        clustering: &C,
        iter: usize,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
    ) {
        info!(
            "Moved {} nodes in iteration {}",
            move_iteration_data.nodes_moved, iter
        );
    }

    fn on_move_phase_ended(
        &mut self,
        clustering: &C,
        iters: usize,
        changed: bool,
        data: &mut Self::MoveIterationData,
    ) {
    }

    fn calculate_delta(
        &self,
        u: NodeKey,
        c: ClusterKey,
        d: ClusterKey,
        graph: &G,
        clustering: &C,
        move_phase_data: &Self::MovePhaseData,
        move_iteration_data: &Self::MoveIterationData,
        move_attempt_data: &Self::MoveAttemptData,
    ) -> f64 {
        let cut_target = move_phase_data.cluster_cut[d];
        let vol_target = move_phase_data.cluster_vol[d];

        let total_cut = move_phase_data.total_cut;
        let total_vol = move_phase_data.total_vol;

        let vol = move_attempt_data.vol;
        let loops = move_attempt_data.loops;
        let weight_to_source = move_attempt_data.weight_to_cluster[c];
        let weight_to_target = move_attempt_data.weight_to_cluster[d];

        // Components of the undirected map equation
        struct DeltaUpdates {
            pub total_cut_after_move: f64,
            pub target_cut_before_move: f64,
            pub target_cut_after_move: f64,
            pub target_cut_plus_vol_before_move: f64,
            pub target_cut_plus_vol_after_move: f64,
        }

        let p_log_p_updates = |updates: DeltaUpdates| DeltaUpdates {
            total_cut_after_move: Self::p_log_p(updates.total_cut_after_move, total_vol),
            target_cut_before_move: Self::p_log_p(updates.target_cut_before_move, total_vol),
            target_cut_after_move: Self::p_log_p(updates.target_cut_after_move, total_vol),
            target_cut_plus_vol_before_move: Self::p_log_p(
                updates.target_cut_plus_vol_before_move,
                total_vol,
            ),
            target_cut_plus_vol_after_move: Self::p_log_p(
                updates.target_cut_plus_vol_after_move,
                total_vol,
            ),
        };

        let mut updates = DeltaUpdates {
            total_cut_after_move: total_cut,
            target_cut_before_move: cut_target,
            target_cut_after_move: cut_target,
            target_cut_plus_vol_before_move: cut_target + vol_target,
            target_cut_plus_vol_after_move: cut_target + vol_target,
        };

        let cut_diff_source = 2.0 * weight_to_source - vol + 2.0 * loops;

        if c != d {
            let cut_diff_target = vol - 2.0 * weight_to_target - 2.0 * loops;

            updates.total_cut_after_move += cut_diff_source + cut_diff_target;

            updates.target_cut_after_move += cut_diff_target;
            updates.target_cut_plus_vol_after_move += cut_diff_target + vol;
        } else {
            updates.target_cut_before_move += cut_diff_source;
            updates.target_cut_plus_vol_before_move += cut_diff_source - vol;
        };

        let updates = p_log_p_updates(updates);

        let delta_cut =
            updates.target_cut_plus_vol_after_move - updates.target_cut_plus_vol_before_move;
        let delta_cut_plus_vol = updates.target_cut_after_move - updates.target_cut_before_move;

        let delta = updates.total_cut_after_move + (delta_cut - (2.0 * delta_cut_plus_vol));

        let same_cluster = c == d;

        trace!("{}", delta);

        delta
    }

    fn get_candidates<'graph>(
        &mut self,
        u: NodeKey,
        graph: &'graph G,
        clustering: &'graph C,
    ) -> Box<dyn Iterator<Item = ClusterKey> + 'graph> {
        Box::new(
            clustering
                .neighboring_clusters(graph, u)
                .unwrap()
                .into_iter(),
        )
    }
}
