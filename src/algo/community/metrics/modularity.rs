use std::collections::HashMap;
use std::marker::PhantomData;

use itertools::Itertools;
use slotmap::{SecondaryMap, SparseSecondaryMap};

use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::algo::community::metrics::coverage::Coverage;
use crate::algo::community::ClusterKey;
use crate::algo::local_moving::LocalMovingObjective;
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::Edge;
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;

pub struct Modularity<D> {
    pub node_volumes: SecondaryMap<NodeKey, f64>,
    pub cluster_volumes: SecondaryMap<ClusterKey, f64>,
    pub total_edge_weight: f64,
    pub divisor: f64,
    pub gamma: f64,
    pub affinities: SecondaryMap<ClusterKey, f64>,
    _phantom: PhantomData<D>,
}

impl<D> Modularity<D>
where
    D: Directedness,
{
    pub fn calculate<N, E, G>(
        graph: &G,
        clustering: &StandardClustering,
    ) -> f64
    where
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
    {
        let cov = Coverage::calculate(graph, clustering);

        let total_edge_weight = graph.total_edge_weight();

        let mut incident_weights = SparseSecondaryMap::new();

        for (cluster, nodes) in clustering.clusters() {
            let cluster_weight: f64 = nodes
                .into_iter()
                .map(|node| graph.weighted_out_degree(*node, true).unwrap())
                .sum();
            incident_weights.insert(cluster, cluster_weight);
        }

        let exp_cov = incident_weights.into_iter().map(|(cluster, weight)| 0.0);

        0.0
    }

    pub fn new<N, E, G>(
        graph: &G,
        clustering: &StandardClustering,
        gamma: f64,
    ) -> Self
    where
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
    {
        let total_edge_weight = graph.total_edge_weight();
        let divisor = 2.0 * total_edge_weight * total_edge_weight;

        let mut node_volumes = SecondaryMap::new();

        for u in graph.nodes() {
            node_volumes.insert(u, graph.weighted_out_degree(u, true).unwrap());
        }

        let mut cluster_volumes = SecondaryMap::new();

        for (cluster, nodes) in clustering.clusters() {
            let cluster_volume = nodes
                .iter()
                .map(|node| graph.weighted_out_degree(*node, true).unwrap())
                .sum::<f64>();
            cluster_volumes.insert(cluster, cluster_volume);
        }

        Self {
            total_edge_weight,
            divisor,
            gamma,
            node_volumes,
            cluster_volumes,
            affinities: SecondaryMap::new(),
            _phantom: PhantomData,
        }
    }
}

impl<D, N, E, G, C> LocalMovingObjective<D, N, E, G, C> for Modularity<D>
where
    D: Directedness,
    N: Node,
    E: Edge,
    G: Nodes<N> + Edges<D, E>,
    C: Clustering
{
    type MovePhaseData = ();
    type MoveIterationData = ();
    type MoveAttemptData = ();

    fn calculate_delta(
        &self,
        u: NodeKey,
        c: ClusterKey,
        d: ClusterKey,
        graph: &G,
        clustering: &C,
        move_phase_data: &Self::MovePhaseData,
        move_iteration_data: &Self::MoveIterationData,
        move_attempt_data: &Self::MoveAttemptData,
    ) -> f64 {
        todo!()
    }

    fn get_candidates<'graph>(
        &mut self,
        u: NodeKey,
        graph: &'graph G,
        clustering: &'graph C,
    ) -> Box<dyn Iterator<Item = ClusterKey> + 'graph> {
        todo!()
    }

    // fn on_move_iteration_started<G>(
    //     &mut self,
    //     graph: &G,
    //     clustering: &Clustering,
    //     data: &mut Self::MoveIterationData,
    // ) where
    //     G: Nodes<N> + Edges<D, E>,
    // {
    //     self.affinities.clear();
    // }
    //
    // fn calculate_delta<G>(
    //     &self,
    //     u: NodeKey,
    //     c: ClusterKey,
    //     d: ClusterKey,
    //     graph: &G,
    //     clustering: &Clustering,
    //     move_phase_data: &Self::MoveIterationData,
    //     move_attempt_data: &Self::MoveAttemptData,
    // ) -> f64
    //     where
    //         G: Nodes<N> + Edges<D, E>,
    // {
    //     let vol_u = self.node_volumes[u];
    //     let vol_c = self.cluster_volumes[c];
    //
    //     let vol_c_minus_u = vol_c - vol_u;
    //     let vol_d = self.cluster_volumes[d];
    //
    //     let affinity = self.affinities[d];
    //
    //     affinity / self.total_edge_weight
    //         + self.gamma * ((vol_c_minus_u - vol_d) - vol_u) / self.divisor
    // }
    //
    // fn get_candidates<G>(
    //     &mut self,
    //     u: NodeKey,
    //     graph: &G,
    //     clustering: &Clustering,
    // ) -> Vec<ClusterKey>
    //     where
    //         G: Nodes<N> + Edges<D, E>,
    // {
    //     graph
    //         .out_edges(u)
    //         .map(|(v, e)| clustering.cluster_of(v))
    //         .unique()
    //         .collect()
    // }
    //
    // fn on_node_moved(
    //     &mut self,
    //     u: NodeKey,
    //     from: ClusterKey,
    //     to: ClusterKey,
    //     move_phase_data: &mut Self::MoveIterationData,
    //     move_attempt_data: &mut Self::MoveAttemptData,
    // ) {
    //     let vol_u = self.node_volumes.get(u).unwrap();
    //
    //     *self.cluster_volumes.get_mut(from).unwrap() -= vol_u;
    //     *self.cluster_volumes.get_mut(to).unwrap() += vol_u;
    // }
}
