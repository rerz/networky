use std::collections::HashMap;

use itertools::Itertools;

use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::Edge;
use crate::graph::node::Node;
use crate::graph::Directedness;

pub struct Coverage;

impl Coverage {
    pub fn calculate<D, N, E, G>(
        graph: &G,
        clustering: &StandardClustering,
    ) -> f64
    where
        D: Directedness,
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
    {
        let total_edge_weight = graph.total_edge_weight();

        let mut intra_cluster_weights = HashMap::new();

        for (cluster, nodes) in clustering.clusters() {
            let weights = intra_cluster_weights.entry(cluster).or_insert(0.0);

            for pair in nodes.into_iter().combinations(2) {
                let (u, v) = (*pair[0], *pair[1]);

                *weights += graph
                    .edges_between(u, v)
                    .unwrap()
                    .map(|(_, _, e)| e.float_weight(graph).unwrap())
                    .sum::<f64>();
            }
        }

        let intra_cluster_weights_sum = intra_cluster_weights.values().sum::<f64>();

        intra_cluster_weights_sum / total_edge_weight
    }
}
