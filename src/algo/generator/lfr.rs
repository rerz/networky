use palette::float::Float;
use rand::prelude::IteratorRandom;
use rand::{thread_rng, Rng};

pub fn zipf(
    alpha: f64,
    x_min: f64,
) -> f64 {
    assert!(x_min >= 1.0);
    assert!(alpha > 1.0);
    let mut rng = thread_rng();

    let a = alpha - 1.0;
    let b = 2.0.powf(a);
    let mut x = 0.0;

    loop {
        let u = 1.0 - rng.gen_range(0.0..1.0);
        let v = rng.gen_range(0.0..1.0);
        x = x_min * (u.powf(-(1.0 / a)));
        let t = (1.0 + (1.0 / x)).powf(a);
        if v * x * (t - 1.0) / (b - 1.0) <= t / b {
            break;
        }
    }

    x
}

pub fn zipf_below(
    gamma: f64,
    x_min: f64,
    threshold: f64,
) -> f64 {
    let mut res = zipf(gamma, x_min);
    while res > threshold {
        res = zipf(gamma, x_min);
    }
    res
}

pub fn power_law(
    gamma: f64,
    low: f64,
    high: f64,
    condition: impl Fn(&[f64]) -> bool,
    length: impl Fn(&[f64]) -> bool,
    max_iter: usize,
) -> Vec<f64> {
    for i in 0..max_iter {
        let mut seq = vec![];
        while !length(&seq) {
            seq.push(zipf_below(gamma, low, high))
        }
        if condition(&seq) {
            return seq;
        }
    }

    panic!("Power iteration failed");
}

pub struct LFR {}

impl LFR {
    fn hurwitz_zeta(
        x: f64,
        q: f64,
        tol: f64,
    ) -> f64 {
        assert!(x > 1.0);
        assert!(q > 0.0);

        let mut z = 0.0;
        let mut z_prev = f64::neg_infinity();
        let mut k = 0.0;
        loop {
            if (z - z_prev).abs() < tol {
                break;
            }

            z_prev = z;
            z += 1.0 / ((k + q).powf(x));
            k += 1.0;
        }

        z
    }

    fn calculate_min_degree(
        gamma: f64,
        avg_degree: usize,
        max_degree: usize,
        tol: f64,
        max_iter: usize,
    ) -> usize {
        let mut top = max_degree as f64;
        let mut bot = 1.0;
        let mut mid = (top - bot) / 2.0 + bot;

        let avg_degree = avg_degree as f64;
        let mut mid_avg = 0.0;

        let mut iter = 0;
        loop {
            if (mid_avg - avg_degree).abs() > tol {
                break;
            }
            if iter > max_iter {
                panic!("Max iters exceeded");
            }

            mid_avg = 0.0;

            for x in (mid as usize)..(max_degree + 1) {
                let x = x as f64;
                mid_avg += (x.powf(-gamma + 1.0)) / Self::hurwitz_zeta(gamma, mid, tol);
            }

            if mid_avg > avg_degree {
                top = mid;
                mid = (top - bot) / 2.0 + bot;
            } else {
                bot = mid;
                mid = (top - bot) / 2.0 + bot;
            }

            iter += 1;
        }

        mid as usize
    }

    pub fn generate(
        n: usize,
        tau_one: f64,
        tau_two: f64,
        mu: f64,
        avg_degree: Option<usize>,
        min_degree: Option<usize>,
        max_degree: Option<usize>,
        min_cluster: f64,
        max_cluster: f64,
        tolerance: f64,
        max_iter: usize,
    ) {
        assert!(tau_one > 1.0);
        assert!(tau_two > 1.0);
        assert!(0.0 <= mu && mu <= 1.0);

        let max_degree = max_degree.unwrap_or(n);

        assert!(0 < max_degree && max_degree <= n);

        let min_degree = match (min_degree, avg_degree) {
            (Some(min), None) => min,
            (None, Some(avg)) => {
                Self::calculate_min_degree(tau_one, avg, max_degree, tolerance, max_iter)
            }
            _ => panic!("Either min or avg degree has to be Some"),
        };

        let deg_seq = {
            let (low, high) = (min_degree as f64, max_degree as f64);

            let cond = |seq: &[f64]| seq.iter().sum::<f64>() as usize % 2 == 0;
            let len = |seq: &[f64]| seq.len() >= n;

            power_law(tau_one, low, high, cond, len, max_iter)
        };

        let com_seq = {
            let (low, high) = (min_cluster, max_cluster);

            let cond = |seq: &[f64]| seq.iter().sum::<f64>() as usize == n;
            let len = |seq: &[f64]| seq.iter().sum::<f64>() >= n as f64;

            power_law(tau_two, low, high, cond, len, max_iter)
        };

        let max_iter = max_iter * 10 * n;
        let clusters = Self::gen_clusters(&deg_seq, &com_seq, mu, max_iter);
    }

    fn gen_clusters(
        deg_seq: &[f64],
        com_seq: &[f64],
        mu: f64,
        max_iter: usize,
    ) {
        //let mut res = vec![];
        let n = deg_seq.len();
        let mut free = vec![0; n];

        for i in 0..max_iter {
            let v = free.pop();
        }
    }
}
