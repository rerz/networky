use std::marker::PhantomData;

use rand::prelude::{IteratorRandom, SliceRandom};
use rand::{thread_rng, Rng};

use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::graph::accessor::{Edges, EdgesMut, Nodes, NodesMut};
use crate::graph::edge::Edge;
use crate::graph::node::Node;
use crate::graph::Directedness;
use itertools::Itertools;

pub mod lfr;

pub trait GraphGenerator<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    type Extra;

    fn generate<G, FN, FE>(
        self,
        node_data: FN,
        edge_data: FE,
    ) -> (G, Self::Extra)
    where
        G: NodesMut<N> + EdgesMut<D, E>,
        FN: Fn(usize) -> N,
        FE: Fn(usize) -> E;
}

pub struct ClusteredRandom<D, N, E> {
    pub n: usize,
    pub k: usize,
    pub p_in: f64,
    pub p_out: f64,
    _phantom: PhantomData<(D, N, E)>,
}

impl<D, N, E> ClusteredRandom<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    pub fn new(
        n: usize,
        k: usize,
        p_in: f64,
        p_out: f64,
    ) -> Self {
        Self {
            n,
            k,
            p_in,
            p_out,
            _phantom: PhantomData,
        }
    }
}

impl<D, N, E> GraphGenerator<D, N, E> for ClusteredRandom<D, N, E>
where
    D: Directedness,
    N: Node,
    E: Edge,
{
    type Extra = StandardClustering;

    fn generate<G, FN, FE>(
        self,
        node_data: FN,
        edge_data: FE,
    ) -> (G, Self::Extra)
    where
        G: NodesMut<N> + EdgesMut<D, E>,
        FN: Fn(usize) -> N,
        FE: Fn(usize) -> E,
    {
        let mut graph = G::with_nodes_and_mapping(self.n, node_data);
        let mut clustering = StandardClustering::new_with(self.k);

        let cluster_keys = clustering.cluster_keys().collect_vec();

        for node in graph.nodes() {
            let cluster = cluster_keys.choose(&mut thread_rng()).unwrap();
            clustering.set(node, *cluster);
        }

        let mut m = 0;

        for (u, v) in graph.node_pairs().collect::<Vec<_>>() {
            let c = clustering.cluster_of(u);
            let d = clustering.cluster_of(v);

            if c == d {
                if thread_rng().gen_bool(self.p_in) {
                    graph.add_edge(u, v, edge_data(m));
                    m += 1;
                }
            } else {
                if thread_rng().gen_bool(self.p_out) {
                    graph.add_edge(u, v, edge_data(m));
                    m += 1;
                }
            }
        }

        (graph, clustering)
    }
}

#[cfg(test)]
mod tests {
    use crate::algo::generator::{ClusteredRandom, GraphGenerator};
    use crate::graph::types::multi::MultiGraph;
    use crate::graph::Undirected;

    #[test]
    fn test() {
        pretty_env_logger::init();
        let gen = ClusteredRandom::<Undirected, (), ()>::new(100, 2, 0.1, 0.005);

        let graph = gen.generate::<MultiGraph<_, _, _>, _, _>(|_| (), |_| ());

        dbg!();
    }
}
