use std::collections::HashMap;

use slotmap::SecondaryMap;

use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::ClusterKey;
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::Edge;
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use crate::algo::community::clustering::Clustering;

pub mod sequential;

pub trait LocalMovingObjective<D, N, E, G, C>
where
    D: Directedness,
    N: Node,
    E: Edge,
    G: Nodes<N> + Edges<D, E>,
    C: Clustering
{
    type MovePhaseData: Default;
    type MoveIterationData: Default;
    type MoveAttemptData: Default;

    fn on_move_phase_started(
        &mut self,
        graph: &G,
        clustering: &C,
        move_phase_data: &mut Self::MovePhaseData,
    ) {
    }

    /// Run once for each move phase
    fn on_move_iteration_started(
        &mut self,
        graph: &G,
        clustering: &C,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
    ) {
    }

    fn on_try_move_node(
        &mut self,
        u: NodeKey,
        c: ClusterKey,
        graph: &G,
        clustering: &C,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
        move_attempt_data: &mut Self::MoveAttemptData,
    ) {
    }

    fn on_node_moved(
        &mut self,
        u: NodeKey,
        from: ClusterKey,
        to: ClusterKey,
        graph: &G,
        clustering: &C,
        best_delta: f64,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
        move_attempt_data: &mut Self::MoveAttemptData,
    ) {
    }

    fn on_move_iteration_ended(
        &mut self,
        clustering: &C,
        iter: usize,
        move_phase_data: &mut Self::MovePhaseData,
        move_iteration_data: &mut Self::MoveIterationData,
    ) {
    }

    fn on_move_phase_ended(
        &mut self,
        clustering: &C,
        iters: usize,
        changed: bool,
        data: &mut Self::MoveIterationData,
    ) {
    }

    fn calculate_delta(
        &self,
        u: NodeKey,
        c: ClusterKey,
        d: ClusterKey,
        graph: &G,
        clustering: &C,
        move_phase_data: &Self::MovePhaseData,
        move_iteration_data: &Self::MoveIterationData,
        move_attempt_data: &Self::MoveAttemptData,
    ) -> f64;

    fn get_candidates<'graph>(
        &mut self,
        u: NodeKey,
        graph: &'graph G,
        clustering: &'graph C,
    ) -> Box<dyn Iterator<Item = ClusterKey> + 'graph>;
}

pub enum OptimizationCriterion {
    Maximize,
    Minimize,
}

pub trait LocalMovingOptimizer<'graph, D, N, E, G, C, O>
where
    D: Directedness,
    N: Node,
    E: Edge,
    G: Nodes<N> + Edges<D, E>,
    C: Clustering,
O: LocalMovingObjective<D, N, E, G, C>
{
    type Config: Clone;

    fn with_initial_clustering(
        graph: &G,
        clustering: &'graph mut C,
        objective: O,
        config: Self::Config,
        criterion: OptimizationCriterion,
    ) -> Self;

    fn move_nodes(
        &mut self,
        graph: &G,
    ) -> bool;

    fn move_node(
        &mut self,
        graph: &G,
        u: NodeKey,
    );

    fn clustering(&self) -> &C;
}
