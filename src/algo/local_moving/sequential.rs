use std::marker::PhantomData;

use itertools::Itertools;
use rand::prelude::SliceRandom;
use rand::thread_rng;

use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::algo::local_moving::{
    LocalMovingObjective, LocalMovingOptimizer, OptimizationCriterion,
};
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::Edge;
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use crate::io::dot_writer::DotWriter;
use palette::float::Float;
use rand::seq::IteratorRandom;

pub struct SequentialLocalMoving<'graph, D, N, E, G, C, O>
where
    D: Directedness,
    N: Node,
    E: Edge,
    G: Nodes<N> + Edges<D, E>,
    C: Clustering,
    O: LocalMovingObjective<D, N, E, G, C>,
{
    pub objective: O,
    pub clustering: &'graph mut C,
    pub config: SequentialLocalMovingConfig,
    pub moved: bool,
    move_phase_data: O::MovePhaseData,
    move_iteration_data: O::MoveIterationData,
    move_attempt_data: O::MoveAttemptData,
    criterion: OptimizationCriterion,
    _phantom: PhantomData<(D, N, E, G)>,
}

#[derive(Clone)]
pub struct SequentialLocalMovingConfig {
    pub randomize_node_order: bool,
    pub max_iter: usize,
}

impl<'graph, D, N, E, G, C, O> LocalMovingOptimizer<'graph, D, N, E, G, C, O>
    for SequentialLocalMoving<'graph, D, N, E, G, C, O>
where
    D: Directedness,
    N: Node,
    E: Edge,
    G: Nodes<N> + Edges<D, E>,
    C: Clustering,
    O: LocalMovingObjective<D, N, E, G, C>,
{
    type Config = SequentialLocalMovingConfig;

    fn with_initial_clustering(
        graph: &G,
        clustering: &'graph mut C,
        objective: O,
        config: Self::Config,
        criterion: OptimizationCriterion,
    ) -> Self {
        Self {
            clustering,
            objective,
            moved: false,
            config,
            move_phase_data: Default::default(),
            move_iteration_data: Default::default(),
            move_attempt_data: Default::default(),
            criterion,
            _phantom: PhantomData,
        }
    }

    fn move_nodes(
        &mut self,
        graph: &G,
    ) -> bool {
        info!("Move phase started");

        let mut changed = false;

        let mut iters = 0;

        self.move_phase_data = O::MovePhaseData::default();
        self.objective
            .on_move_phase_started(graph, &self.clustering, &mut self.move_phase_data);

        for iter in 0..self.config.max_iter {
            warn!("Clustering iteration: {}", iter);

            self.moved = false;

            self.move_iteration_data = O::MoveIterationData::default();
            self.objective.on_move_iteration_started(
                graph,
                &self.clustering,
                &mut self.move_phase_data,
                &mut self.move_iteration_data,
            );

            if self.config.randomize_node_order {
                let mut nodes = graph.nodes().collect_vec();
                nodes.shuffle(&mut thread_rng());

                for u in nodes {
                    self.move_node(&graph, u);
                }
            } else {
                for u in graph.nodes() {
                    self.move_node(&graph, u);
                }
            }

            self.objective.on_move_iteration_ended(
                &self.clustering,
                iter,
                &mut self.move_phase_data,
                &mut self.move_iteration_data,
            );

            iters += 1;
            changed |= self.moved;

            if !self.moved {
                break;
            }
        }

        self.objective.on_move_phase_ended(
            &self.clustering,
            iters,
            changed,
            &mut self.move_iteration_data,
        );

        changed
    }

    fn move_node(
        &mut self,
        graph: &G,
        u: NodeKey,
    ) {
        let c = self.clustering.cluster_of(u).unwrap();
        let mut best_delta = match self.criterion {
            OptimizationCriterion::Maximize => f64::neg_infinity(),
            OptimizationCriterion::Minimize => f64::infinity(),
        };

        let mut best_cluster = c;

        self.move_attempt_data = O::MoveAttemptData::default();

        self.objective.on_try_move_node(
            u,
            c,
            graph,
            &self.clustering,
            &mut self.move_phase_data,
            &mut self.move_iteration_data,
            &mut self.move_attempt_data,
        );

        for d in self.objective.get_candidates(u, graph, &self.clustering) {
            let delta = self.objective.calculate_delta(
                u,
                c,
                d,
                graph,
                &self.clustering,
                &self.move_phase_data,
                &self.move_iteration_data,
                &self.move_attempt_data,
            );

            let delta_improved = match self.criterion {
                OptimizationCriterion::Maximize => delta > best_delta,
                OptimizationCriterion::Minimize => delta < best_delta,
            };

            if delta_improved {
                best_delta = delta;
                best_cluster = d;
            }
        }

        if best_cluster != c {
            //DotWriter::write("current.dot", graph, Some(&self.clustering));

            self.clustering.set(u, best_cluster);
            self.objective.on_node_moved(
                u,
                c,
                best_cluster,
                graph,
                &self.clustering,
                best_delta,
                &mut self.move_phase_data,
                &mut self.move_iteration_data,
                &mut self.move_attempt_data,
            );
            self.moved = true;
        }
    }

    fn clustering(&self) -> &C {
        &self.clustering
    }
}
