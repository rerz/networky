pub trait Force {
    fn calculate(&mut self, alpha: f64);

    fn init() -> Self where Self: Sized;
}

pub struct LinkForce {
    pub links: Vec<()>,
}

impl Force for LinkForce {
    fn calculate(&mut self, alpha: f64) {
        todo!()
    }

    fn init() -> Self where Self: Sized {
        todo!()
    }
}


pub struct SimNode {}


pub struct Simulation {
    tick: usize,
    nodes: Vec<usize>,
    forces: Vec<Box<dyn Force>>,
    alpha: f64,
    alpha_min: f64,
    alpha_decay: f64,
    alpha_target: f64,
}

impl Simulation {
    pub fn new(alpha: f64, alpha_min: f64, alpha_decay: f64, alpha_target: f64) -> Self {
        Self {
            tick: 0,
            nodes: vec![],
            forces: vec![],
            alpha,
            alpha_min,
            alpha_decay,
            alpha_target,
        }
    }

    pub fn add_force(&mut self, force: Box<dyn Force>) {
        self.forces.push(force);
    }

    pub fn tick(&mut self) {
        self.alpha += (self.alpha_target - self.alpha) * self.alpha_decay;

        for force in &mut self.forces {
            force.calculate(self.alpha);
        }

        for node in &self.nodes {}
    }
}