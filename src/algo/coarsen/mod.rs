use std::marker::PhantomData;

use itertools::Itertools;
use slotmap::SecondaryMap;

use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::graph::accessor::{Edges, EdgesMut, Nodes, NodesMut};
use crate::graph::edge::Edge;
use crate::graph::node::{Node, NodeKey};
use crate::graph::types::multi::MultiGraph;
use crate::graph::types::simple::SimpleGraph;
use crate::graph::{Directedness, Undirected};
use crate::storage::StorageError;

pub struct ClusterCoarsening<D, N, E> {
    _phantom: PhantomData<(D, N, E)>,
}

impl<N, E> ClusterCoarsening<Undirected, N, E>
where
    N: Node,
    E: Edge,
{
    pub fn coarsen<G, F, T, C>(
        graph: &G,
        clustering: &C,
        mut node_aggregation: F,
    ) -> Result<SimpleGraph<Undirected, T, f64>, StorageError>
    where
        G: NodesMut<N> + EdgesMut<Undirected, E>,
        F: FnMut(NodeKey, &mut T),
        T: Node + Default,
        C: Clustering,
    {
        let mut coarse = SimpleGraph::new();

        let mut cluster_to_meta_node = SecondaryMap::new();

        for (cluster, nodes) in clustering.clusters() {
            let mut meta_node_value = T::default();

            for node in nodes {
                node_aggregation(*node, &mut meta_node_value);
            }

            let meta_node = coarse.add_node(meta_node_value);

            cluster_to_meta_node.insert(cluster, meta_node);
        }

        for (u, v, e) in graph.edges() {
            let c = clustering.cluster_of(u).unwrap();
            let d = clustering.cluster_of(v).unwrap();

            let w = e.float_weight(graph)?;

            let u_meta = cluster_to_meta_node[c];
            let v_meta = cluster_to_meta_node[d];

            coarse.add_or_update_edge(u_meta, v_meta, w, |weight| *weight += w);
        }

        assert_eq!(coarse.n(), clustering.num_clusters());
        assert_eq!(coarse.total_edge_weight(), graph.total_edge_weight());

        Ok(coarse)
    }
}
