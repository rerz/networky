use slotmap::{Key, SecondaryMap, SparseSecondaryMap};
use std::iter::FromIterator;

pub trait SecondaryMapExt<K, V> where K: Key {
    fn from_iter_map<I, F>(iter: I, mut map: F) -> Self where I: IntoIterator<Item = K>, F: FnMut(K) -> V, Self: FromIterator<(K, V)> {
        Self::from_iter(
            iter
                .into_iter()
                .map(|key| (key, map(key)))
        )
    }
}

impl<K, V> SecondaryMapExt<K, V> for SecondaryMap<K, V> where K: Key {}

impl<K, V> SecondaryMapExt<K, V> for SparseSecondaryMap<K, V> where K: Key {}

#[macro_export]
macro_rules! ungraph {
    ($graph:ident, $($source:literal -- $target:literal),*) => {
        {
            let mut _graph = $graph::<Undirected, _, _>::new();
            $(
                _graph.add_new_edge($source, $target, ());
            )*
            _graph
        }
    };
}

#[macro_export]
macro_rules! digraph {
    ($graph:ident, $($source:literal -- $target:literal),*) => {
        {
            let mut _graph = <$graph>::<Directed, _, _>::new();
            $(
                _graph.add_new_edge($source, $target, ());
            )*
            _graph
        }
    };
}

#[macro_export]
macro_rules! clustering {
    ($graph:expr, $($cluster:expr),*) => {
        {
            let graph = $graph;
            let mut clustering = crate::algo::community::Clustering::new();
            $({
                let mut cluster = clustering.new_cluster();
                for node in std::array::IntoIter::new($cluster) {
                    let node = graph.get_or_add_node(node);
                    clustering.set(node, cluster);
                }
            })*
            clustering
        }
    };
}
