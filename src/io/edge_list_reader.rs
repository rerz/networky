use std::fmt::Debug;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;
use std::str::FromStr;

use itertools::Itertools;

use crate::graph::accessor::{EdgesMut, NodesMut};
use crate::graph::edge::Edge;
use crate::graph::node::Node;
use crate::graph::Directedness;

pub struct EdgeListReader;

pub struct EdgeListReaderConfig {
    pub separator: String,
}

impl EdgeListReader {
    pub fn read<D, E, G>(path: impl AsRef<Path>) -> G
    where
        D: Directedness,
        E: Edge + Default,
        G: NodesMut<usize> + EdgesMut<D, E>,
    {
        info!("Reading graph from {:?}", path.as_ref());

        let reader = BufReader::new(File::open(path).unwrap());

        let mut graph = G::new();

        let mut lines = reader.lines();

        let (hint, upper) = lines.size_hint();

        for (idx, line) in lines.enumerate() {
            let line = line.unwrap();
            let parts = line.split(" ").collect_vec();
            let (source, target) = (
                parts[0].parse::<usize>().unwrap(),
                parts[1].parse::<usize>().unwrap(),
            );

            graph.add_new_edge(source, target, Default::default());
        }

        info!(
            "Read graph with {} nodes and {} edges",
            graph.n(),
            graph.m()
        );

        graph
    }
}
