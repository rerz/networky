use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::Edge;
use crate::graph::node::{Identifiable, Node};
use crate::graph::Directedness;
use std::io::{LineWriter, Write};
use std::path::Path;

pub struct ClusteringWriter;

impl ClusteringWriter {
    pub fn write_clustering<D, N, E, G, C>(
        path: impl AsRef<Path>,
        graph: &G,
        clustering: &C,
    ) where
        D: Directedness,
        N: Node + Identifiable,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
        C: Clustering,
    {
        let mut file = std::fs::File::create(path).unwrap();
        let mut writer = LineWriter::new(file);

        for (idx, (_, nodes)) in clustering.clusters().enumerate() {
            for node in nodes {
                let node = graph.node_data(*node).unwrap();
                writer
                    .write_all(format!("{} {}\n", node.id(), idx).as_bytes())
                    .unwrap();
            }
        }
    }
}
