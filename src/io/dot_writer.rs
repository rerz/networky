use std::marker::PhantomData;
use std::path::Path;

use itertools::Itertools;
use palette::encoding::Srgb;
use palette::rgb::Rgb;
use palette::white_point::A;
use palette::IntoColor;
use slotmap::SecondaryMap;

use crate::algo::community::clustering::standard::StandardClustering;
use crate::algo::community::clustering::Clustering;
use crate::algo::community::ClusterKey;
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::{Edge, EdgeKey, EdgeRef};
use crate::graph::node::{Identifiable, Node, NodeKey};
use crate::graph::Directedness;

#[derive(Clone, Default)]
pub struct DotCluster {
    pub intra_edges: Vec<EdgeRef>,
    pub nodes: Vec<NodeKey>,
}

pub struct DotWriter<D>
where
    D: Directedness,
{
    _phantom: PhantomData<D>,
}

impl<D> DotWriter<D>
where
    D: Directedness,
{
    pub fn write<N, E, G>(
        path: impl AsRef<Path>,
        graph: &G,
        clustering: Option<&StandardClustering>,
    ) where
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
    {
        let arrow = if D::is_directed() { "->" } else { "--" };

        let graph_type = if D::is_directed() { "digraph" } else { "graph" };

        let mut clusters = SecondaryMap::new();

        let edges = if let Some(clustering) = clustering {
            for (cluster, nodes) in clustering.clusters() {
                clusters.insert(
                    cluster,
                    DotCluster {
                        nodes: nodes.iter().cloned().collect_vec(),
                        intra_edges: vec![],
                    },
                );
            }

            let (intra, inter) = clustering.intra_and_inter_cluster_edges(graph);

            for (u, v, e) in intra {
                let cluster = clustering.cluster_of(u).unwrap();

                clusters
                    .get_mut(cluster)
                    .unwrap()
                    .intra_edges
                    .push((u, v, e));
            }

            inter
        } else {
            graph.edges().collect()
        };

        let mut node_to_id = SecondaryMap::new();
        let mut nodes_seen = 0usize;

        for node in graph.nodes() {
            if !node_to_id.contains_key(node) {
                node_to_id.insert(node, nodes_seen);
                nodes_seen += 1;
            }
        }

        let make_edge = |(u, v, e): EdgeRef| {
            let id_u = node_to_id[u];
            let id_v = node_to_id[v];
            format!(
                "\t{} {} {} [label = \"{}\"];",
                id_u,
                arrow,
                id_v,
                format!("{}", e.float_weight(graph).unwrap())
            )
        };

        let make_subgraph = |idx: usize, cluster: DotCluster| {
            let edges = cluster.intra_edges.into_iter().map(make_edge).join("\n");

            let single_node = if cluster.nodes.len() == 1 {
                format!("{}", node_to_id[cluster.nodes[0]])
            } else {
                "".into()
            };

            format!(
                "subgraph cluster_{:?} {{\n {}\n {}\n\t}}",
                idx, single_node, edges
            )
        };

        let subgraphs = clusters
            .into_iter()
            .enumerate()
            .map(|(idx, (key, cluster))| make_subgraph(idx, cluster))
            .join("\n");

        let edges = edges.into_iter().map(make_edge).join("\n");

        let dot = format!("{} {{\n {}\n {}\n}}", graph_type, subgraphs, edges);

        std::fs::write(path, dot).expect("Could not write dot file");
    }
}

#[cfg(test)]
mod tests {
    use crate::algo::generator::{ClusteredRandom, GraphGenerator};
    use crate::graph::accessor::{EdgesMut, NodesMut};
    use crate::graph::types::multi::MultiGraph;
    use crate::graph::types::simple::SimpleGraph;
    use crate::graph::Undirected;
    use crate::io::dot_writer::DotWriter;

    #[test]
    fn test_writer() {
        let gen = ClusteredRandom::<Undirected, usize, ()>::new(100, 4, 0.8, 0.02);
        let (graph, clustering): (MultiGraph<_, _, _>, _) = gen.generate(|idx| idx, |_| ());

        DotWriter::write("out.dot", &graph, None);
    }
}
