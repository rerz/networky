use crate::algo::community::{ClusterKey, ClusteringObserver};
use crate::graph::accessor::{Edges, Nodes};
use crate::graph::edge::Edge;
use crate::graph::node::{Node, NodeKey};
use crate::graph::Directedness;
use serde::{Deserialize, Serialize};

pub mod clustering_writer;
pub mod dot_writer;
pub mod edge_list_reader;

pub struct ForceGraphClusteringClient {
    reqwest_client: reqwest::Client,
}

impl ForceGraphClusteringClient {
    pub fn new() -> Self {
        Self {
            reqwest_client: reqwest::ClientBuilder::new().build().unwrap(),
        }
    }

    pub fn set_graph<D, N, E, G>(
        &mut self,
        graph: &G,
    ) where
        D: Directedness,
        N: Node,
        E: Edge,
        G: Nodes<N> + Edges<D, E>,
    {
    }
}

#[derive(Deserialize, Serialize)]
pub struct ClusteringChange {
    node: String,
    from: String,
    to: String,
}

impl ClusteringObserver for ForceGraphClusteringClient {
    fn on_cluster_changed(
        &mut self,
        u: NodeKey,
        old_cluster: ClusterKey,
        new_cluster: ClusterKey,
    ) {
        futures::executor::block_on(async {
            self.reqwest_client
                .post("http://localhost:8080/change")
                .json(&ClusteringChange {
                    node: format!("{:?}", u),
                    from: format!("{:?}", old_cluster),
                    to: format!("{:?}", new_cluster),
                })
                .send()
                .await
                .unwrap();
        });
    }
}
