import {AfterViewInit, Component, ElementRef, Injectable, OnInit, ViewChild} from '@angular/core';
import {default as ForceGraph, ForceGraph3DInstance} from '3d-force-graph';
import {filterNilValue, Query, Store, StoreConfig} from "@datorama/akita";

interface Edge {
  source: string;
  target: string;
}

interface Node {
  id: string;
  cluster: string | null;
}

interface GraphDTO {
  nodes: Array<string>,
  edges: Array<[string, string]>
}

interface ClusteringDTO {
  [key: string]: string;
}

interface Clustering extends ClusteringDTO {
}

interface Graph {
  nodes: Array<Node>,
  edges: Array<Edge>,
}

interface GraphViewerState {
  currentGraph: Graph;
  currentClustering: Clustering;
}

@StoreConfig({name: 'graphViewer'})
@Injectable()
export class GraphViewerStore extends Store<GraphViewerState> {
  constructor() {
    super({});
  }
}

@Injectable()
export class GraphViewerQuery extends Query<GraphViewerState> {
  graph$ = this.select(state => state.currentGraph);
  clustering$ = this.select(state => state.currentClustering);

  constructor(protected store: GraphViewerStore) {
    super(store);
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild("forceGraph")
  private viewerElement!: ElementRef<HTMLDivElement>;

  private graph!: ForceGraph3DInstance;

  constructor(private viewerStore: GraphViewerStore, private viewerQuery: GraphViewerQuery) {
    viewerQuery.clustering$.pipe(filterNilValue()).subscribe(clustering => {
      console.log("In clustering update handler")
      console.log(clustering);
      this.viewerStore.update(state => ({
        currentGraph: {
          ...state.currentGraph,
          nodes: state.currentGraph.nodes.map(node => ({ ...node, cluster: clustering[node.id] })),
        }
      }));
    });
    viewerQuery.graph$.pipe(filterNilValue()).subscribe(graph => {
      console.log("In graph update handler");
      console.log(graph);

      if (!graph.nodes[0].cluster) {
        return;
      }

      this.graph.graphData({nodes: graph.nodes, links: graph.edges});
    });
  }

  ngAfterViewInit() {
    this.graph = ForceGraph({})(this.viewerElement.nativeElement);

    const linkForce = (this.graph.d3Force('link') as any).distance((link: any) => {
      const clustering = this.viewerQuery.getValue().currentClustering;
      if (clustering[link.source] === clustering[link.target]) {
        return 0.001;
      } else {
        return 100000;
      }
    });

    this.graph.width(1280).height(720).nodeAutoColorBy('cluster').linkOpacity(0.2).linkWidth(0.5);
  }

  async loadGraph(target: any) {
    console.log(target);
    const files: FileList = target.files;
    const file = files.item(0);

    const jsonString = await file?.text() ?? JSON.stringify({nodes: [], edges: []});
    const graphDTO = JSON.parse(jsonString) as GraphDTO;
    const graph: Graph = {
      nodes: graphDTO.nodes.map(id => ({id, cluster: null})),
      edges: graphDTO.edges.map(([source, target]) => ({source, target}))
    };

    this.viewerStore.update({currentGraph: graph});
  }

  async loadClustering(target: any) {
    const files: FileList = target.files;
    const file = files.item(0);

    const jsonString = await file?.text() ?? JSON.stringify({});

    this.viewerStore.update({currentClustering: JSON.parse(jsonString) as Clustering})
  }

  ngOnInit(): void {

  }
}
