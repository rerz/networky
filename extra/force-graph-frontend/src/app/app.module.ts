import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import {AppComponent, GraphViewerQuery, GraphViewerStore} from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [GraphViewerStore, GraphViewerQuery],
  bootstrap: [AppComponent]
})
export class AppModule { }
