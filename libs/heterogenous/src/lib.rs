use networky::graph::node::Node;

pub trait HetEdge {
    type Source: Node;
    type Target: Node;
}

#[cfg(test)]
mod tests {
    use networky::graph::edge::{Edge, EdgeData};
    use networky::graph::node::{Node, NodeData};

    use crate::HetEdge;
    use networky::graph::types::simple::SimpleGraph;
    use networky::graph::accessor::NodesMut;
    use networky::graph::Undirected;

    pub struct Author;

    impl Node for Author {
        type Data = Self;

        fn data(&self) -> &Self::Data {
            self
        }
    }

    pub struct Paper;

    impl Node for Paper {
        type Data = Self;

        fn data(&self) -> &Self::Data {
            self
        }
    }

    pub struct Published;

    impl HetEdge for Published {
        type Source = Author;
        type Target = Paper;
    }

    pub struct CoAuthor;

    impl HetEdge for CoAuthor {
        type Source = Author;
        type Target = Author;
    }

    #[derive(Graph)]
    #[graph = MultiGraph]
    #[nodes = [Author, Paper]]
    #[edges = [Published, CoAuthor]]
    struct MyCoolBusinessGraph;

    #[derive(Clone)]
    pub struct DirectedEdge;
    #[derive(Clone)]
    pub struct UndirectedEdge;

    #[derive(Clone)]
    pub enum Edges {
        Directed(DirectedEdge),
        Undirected(UndirectedEdge),
    }

    #[test]
    fn it_works() {
        let mut het_graph = SimpleGraph::<Undirected, (), Edges>::new();
    }
}
