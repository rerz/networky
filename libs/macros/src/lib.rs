use darling::FromDeriveInput;
use darling::FromMeta;
use proc_macro::TokenStream;
use syn::DeriveInput;

#[proc_macro_derive(HetGraph, attributes(graph))]
pub fn derive_het_graph(input: TokenStream) -> TokenStream {
    let input = syn::parse::<DeriveInput>(input).unwrap();

    (quote::quote! {}).into()
}
